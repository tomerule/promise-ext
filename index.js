const status = {
    SUCCESS: "success",
    FAILURE: "failure"
};

exports.promiseExt = {
    status: status,
    waitForAll: (promises) => {
        if (!promises || typeof promises[Symbol.iterator] !== 'function') {
            return Promise.reject();
        }

        if (promises.length === 0) {
            return Promise.resolve([]);
        }

        return Promise.all(
            promises.map((promise) => {
                return new Promise((resolve) => {
                    promise.then(result => {
                        resolve({
                            status: status.SUCCESS,
                            result: result
                        });
                    }, err => {
                        resolve({
                            status: status.FAILURE,
                            error: err
                        });
                    });
                });
            })
        );
    },
    joinPooledTasks: (worker, poolSize, onTaskFinished) => {

        let didEnqueAllTasks = false;
        let taskIndex = -1;

        const onTaskEnd = (index, isSuccessful, result, resolve) => {
            const currentResultObject = {
                index: index,
                status: isSuccessful ? status.SUCCESS : status.FAILURE,
            };
            if (isSuccessful) {
                currentResultObject.result = result;
            } else {
                currentResultObject.error = result;
            }

            if (onTaskFinished) {
                onTaskFinished(currentResultObject);
            }

            const nextTask = createNextTask(taskIndex + 1);
            if (nextTask === null) {
                resolve([currentResultObject]);
            } else {
                taskIndex += 1;
                nextTask.then((nextTaskResult) => {
                    resolve([currentResultObject].concat([].concat.apply([], nextTaskResult)));
                });
            }
        };

        const createNextTask = (index) => {
            let nextTask = worker(index);
            if (nextTask === null) {
                didEnqueAllTasks = true;
                return null;
            }

            const isPromise = typeof nextTask.then == 'function';
            if (!isPromise) {
                nextTask = Promise.resolve(nextTask());
            }

            return new Promise((resolve) => {
                nextTask.then(result => {
                        onTaskEnd(index, true, result, resolve);
                    },
                    (err) => {
                        onTaskEnd(index, false, err, resolve);
                    });
            });
        };

        const tasks = [];
        do {
            taskIndex += 1;
            const nextTask = createNextTask(taskIndex);
            if (nextTask !== null) {
                tasks.push(nextTask);
            }
        } while (taskIndex < poolSize && !didEnqueAllTasks);

        return Promise.all(tasks).then((results) => {
            return [].concat.apply([], results).sort((x, y) => x.index - y.index);
        });
    }
}

Promise.waitForAll = exports.promiseExt.waitForAll;
Promise.joinPooledTasks = exports.promiseExt.joinPooledTasks;